# Développement en local avec Docker

Prérequis: Une distribution GNU/Linux avec [Docker](https://docs.docker.com/install/linux/docker-ce/debian/) ou [Podman](https://podman.io/getting-started/installation) installé .

1. Cloner ce dépôt et se placer dedans
```
git clone https://gitlab.utc.fr/elus_etu/website && cd website
```
2. Construire une image docker `elus-dev` à partir de `Dockerfile.dev`
```
docker build -t elus-dev -f Dockerfile.dev
```
3. Lancer un conteneur temporaire pour servir le site
```
docker run \
    --rm \
    -it \
    -v .:/root/:z \
    -v /etc/localtime:/etc/localtime:ro \
    -p 4000:4000 \
    elus-dev
```
(L'option `:z` permet d'éviter des problèmes de permissions sur des systèmes avec SELinux activé)

Le site est maintenant disponible à l'adresse suivante: http://127.0.0.1:4000

À chaque modification des sources, il sera mis à jour.

Si vous préférez avoir un shell pour lancer des commandes `jekyll`, vous pouvez exécuter:

```
docker run \
    --rm \
    -it \
    -v .:/root/:z \
    -v /etc/localtime:/etc/localtime:ro \
    -p 4000:4000 \
    elus-dev \
    bash
```
