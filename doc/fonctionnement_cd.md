# Résumé du fonctionnement du CD (_Continuous Deployment_)

Gitlab dispose de fonctionnalités [CI/CD](https://docs.gitlab.com/ce/ci/) qui permettent d'automatiser certaines taches.

* L'intégration continue (CI) consiste à compiler, tester et valider les changements après chaque commit
* Le déploiement continu (CD) consiste à déployer une application en production après chaque commit

Dans notre cas, on fait du «demi CD», puisque qu'on lance les _pipelines_ manuellement.

Lorsqu'on lance une _pipeline_, (si j'ai bien compris) GitLab réalise les actions suivantes:

 * Création d'un `gitlab-runner`: il s'agit d'un conteneur Docker qui va servir de base
 * Création d'un conteneur à partir de `ruby:2.7-alpine3.11`, à l'intérieur du `gitlab-runner` (oui, c'est du _Docker in Docker_)
 * Exécution des commandes définies dans [.gitlab-ci.yml](/.gitlab-ci.yml):
  * Installation des dépendances système: `apk add --no-cache make gcc libc-dev curl g++ git`
   * Installation des dépendances Ruby (définies dans le [Gemfile](/Gemfile)): `bundle install`
   * Build du site: `bundle exec jekyll build -d public`
* Sauvegarde du résultat (le dossier `/public`) dans un _artifact_.
