# Mettre à jour le site des élu·e·s en 3 simples étapes

_Ce tutoriel s'adresse aux personnes ayant un compte sur gitlab.utc.fr. Avant de commencer, vous aurez besoin des accès à ce dépôt. [Demander les accès](mailto:amaldona@etu.utc.fr?subject=[site_elu-e-s][demande_acces])_

_En créant ou en modifiant un post, vous acceptez de le partager sous les termes de la licence [Creative Commons Attribution-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/deed.fr)_

## 1) Créer un post depuis l'interface web GitLab

1. Lancez le [Web IDE](https://gitlab.utc.fr/-/ide/project/elus_etu/website/edit/master/-/) de GitLab.
2. Créez un nouveau fichier et nommez le selon le modèle `_posts/aaaa-mm-jj-titre-du-post.md` (exemple: `_posts/2020-03-25-demo-post.md`)
3. Copiez et adaptez le _header_ suivant:
```yaml
---
title: "Titre du post"
layout: post
date: 2099-01-01 12:00                           # Laissez 2099 si c'est un brouillon
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: false                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
---
```
  * Liste des comptes disponibles: `eluscevu`, `elusca`, `elusced`, `eluscpm`, `eluscs`, `elusgi`, `elusgu`, `elusgp`, `elusgb`, `elusim`, `elustc` + des comptes perso que vous pouvez me demander d'ajouter (ex: `amaldona`)
4. Écrivez le post en Markdown (même syntaxe que sur Mattermost)
  * Pour ajouter des images, sauvegardez-les dans `/assets/images/posts/`. Vous pouvez les insérer avec la syntaxe `![][{{site.url}}/assets/images/posts/mon_image.png]`. Vérifiez que l'image est sous licence libre (si la licence est différente de la CC BY-SA 4.0, veuillez le préciser).
  * Pour les titres, commencez à partir du niveau 2 (`##`)
5. Vérifiez que tout est OK dans l'onglet `Preview Markdown`

![](/assets/images/doc/Screenshot_IDE_1.png)

## 2) Enregistrer son travail

Si vous n'avez jamais utilisé `git`, pas d'inquiétude ! Vous avez juste besoin de savoir qu'un _commit_ c'est une sauvegarde d'un ou plusieurs fichiers.

1. Appuyez sur `Commit...`
2. Vous pouvez laisser le message par défaut
3. Sélectionnez `Commit to master branch`
4. Appuyez sur `Commit`

![](/assets/images/doc/Screenshot_IDE_2.png)

## 3) Lancer une mise à jour du site

GitLab permet d'exécuter automatiquement des actions après chaque commit. Ces actions sont appelées _pipelines_. Souvent, elles sont lancées automatiquement, mais dans notre cas, elles sont déclenchées manuellement,seulement quand c'est nécessaire, histoire de consommer moins de ressources 🌱

Pour lancer la mise à jour, rendez-vous ici: https://gitlab.utc.fr/elus_etu/website/pipelines

![](/assets/images/doc/Screenshot_Pipelines_1.png)

Cliquez sur le bouton Play à droite du dernier commit, puis sur `pages`.

![](/assets/images/doc/Screenshot_Pipelines_2.png)

Le déploiement est maintenant en cours. Il est possible de l'annuler en cliquant sur la croix rouge à droite. Pour l'instant, ce n'est pas du tout optimisé, donc ça prend une bonne quinzaine de minutes.

Si tout ce passe bien, vous devriez voir une balise `passed`:

![](/assets/images/doc/Screenshot_Pipelines_3.png)

Votre version est maintenant en ligne !

## Pour aller plus loin

Vous pouvez aussi ajouter les éléments suivants dans le _header_
```yaml
tag:
  - CÉVU
  - GI
description: Un résumé rapide du post            # Apparaît lorsque vous partagez le lien
```

Guides détaillés sur ce qu'on peut faire en Markdown:
* [Markdown Common Elements](https://koppl.in/indigo/markdown-common-elements/)
* [Markdown Extra Components](https://koppl.in/indigo/markdown-extra-components/)
