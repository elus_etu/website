#!/bin/bash
#
################################################################################
#
# Logo generator
# Copyright (C) 2020  Andrés Maldonado
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
################################################################################
#
#
# Requirements:
#
# * 'inkscape'
# * 'optipng'
# * 'iconv' (already in most distros)
# * (optional) 'bash' >= 4.0
#

set -e

# Logos and authors to generate

list="CÉVU CA CED CPM CS GI GU GP GB IM TC"
list_no_accent=`echo $list |  iconv --from-code=utf8 --to-code=ascii//TRANSLIT`

# Change directory to the parent folder of this script

source_dir="$( dirname "${BASH_SOURCE[0]}" )"
cd "$source_dir"

# Search dependencies

INKSCAPE=$(command -v inkscape) || true
OPTIPNG=$(command -v optipng) || true
ICONV=$(command -v iconv) || true

# If there are missing dependencies, print install instructions

if [[ ! $INKSCAPE || ! $OPTIPNG || ! $ICONV ]]; then

  echo -e "\nError: The following dependencies are missing:\n"

  if [ ! $INKSCAPE ]; then
    echo "\t* 'inkscape'"
    install_list="inkscape"
  fi
  if [ ! $OPTIPNG ]; then
    echo "\t* 'optipng'"
    install_list="$install_list optipng"
  fi
  if [ ! $ICONV ]; then
    echo "\t* 'iconv'"
    install_list="$install_list iconv"
  fi

  echo -e "\nIf you are on Fedora, you can install missing dependencies by running:\n"

  if [ $install_list ]; then
    echo "\$ sudo dnf install $install_list"
  fi

  exit 1

fi


function echo_blue {
  echo -e "\033[1;34m\n$1\n\033[0m"
}

function no_accent {
  echo $1 |  iconv --from-code=utf8 --to-code=ascii//TRANSLIT
}

# Main program

echo_blue "(1/5): Generate SVG IMAGES"
for i in $list; do
  sed "s/FE 205A/$i/" all.svg > $(no_accent $i).svg
done

echo_blue "(2/5): Transform SVG to PNG"
for i in $list_no_accent; do
  inkscape --export-file=$i.png $i.svg
done
inkscape --export-file=all.png all.svg

echo_blue "(3/5): Optimize PNG"
optipng *.png

echo_blue "(4/5): Move PNG to 'assets/images/profile_pictures/'"
mv *.png ../assets/images/profile_pictures/

echo_blue "(5/5): Cleanup SVG IMAGES"
for i in $list_no_accent; do
  rm $i.svg
done

echo_blue "Build successfull !"

# Check if 'bash' version is >= 4.0 (needed for the '${i,,}' syntax)
if [ ${BASH_VERSION:0:1} -gt 3 ]; then
  echo_blue "Final step: add this to your '_config.yml' :"

  echo "authors:"
  for i in $list; do
    i_lowercase_na=$(no_accent ${i,,})
    echo "    elus${i_lowercase_na}:"
    echo "        name: Élu·e·s étu $i"
    echo "        external-image: false"
    echo "        picture: assets/images/profile_pictures/$(no_accent $i).png"
    echo "        email: elus${i_lowercase_na}@utc.fr"
  done

  echo_blue "List of available accounts:"
  for i in $list_no_accent; do
    printf "\`elus${i,,}\`, "
  done
  echo "" #newline
fi
