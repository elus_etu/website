---
title: "Newsletter 2"
layout: post
date: 2020-03-31 12:40                           # Laissez 2099 si c'est un brouillon
image: /assets/images/profile_pictures/all.png   # Apparaît lorsque vous partagez le lien
headerImage: true                               # Mettez 'true' pour mettre l'image précédente en entête du post
category: blog
author: eluscevu
---

Salut à tou.te.s !

Voici un message plutôt important de vos représentants, les élu.e.s étudiant.e.s.

**Nous avons résumé notre travail de ces dernières semaines !** Toutes les informations concernant nos actualités sont accessibles [EN CLIQUANT ICI](https://cloud.elus-etu.utc.fr/index.php/s/Qg4YWoHfirss5wm) !

Au programme : _Informations COVID 19, Collectif pour Ingénierie durable, et news du CÉVU, CA et des bureaux de département GB, GI, GU, GP, IM et TSH !_

Pour les intéressé.e.s, un **document détaillé de présentation du Collectif pour l'ingénierie durable** est disponible [ICI](https://cloud.elus-etu.utc.fr/index.php/s/KMGjNAQzFNx9DND) !


Nous tenions à vous dire que la période que nous traversons actuellement est difficile pour tout le monde, nous sommes à votre écoute si vous rencontrez des problèmes avec vos cours à distance, votre stage, le télétravail ou autre !

Vous pouvez nous contacter par mail... :
* Élu.e.s CÉVU ou CA : <eluscevu@utc.fr>, <elusca@utc.fr>
* Élu.e.s de ta branche ou de ton cursus :
  * <elusgb@utc.fr>  
  * <elusgi@utc.fr>  
  * <elusgsu@utc.fr>  
  * <elusgp@utc.fr>  
  * <elusim@utc.fr>  
  * <elustc@utc.fr>  
  * <eluscpm@utc.fr>  
  * <elustsh@utc.fr>  

... mais également nous rejoindre sur mattermost en rejoignant l'équipe publique des élu.e.s : <https://team.picasoft.net/signup_user_complete/?id=mbhk9msk878h9dk4zmpxefmako>


Nous vous souhaitons beaucoup de courage pour les semaines à venir,


Les 60 (!) élu.e.s étudiant.e.s CÉVU, CA, GP, GB, GI, GU, IM et TSH
