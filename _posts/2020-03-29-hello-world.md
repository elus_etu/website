---
title: "Le nouveau site des élu·e·s est là !"
layout: post
date: 2020-03-29 20:17
image: /assets/images/posts/pc3.png  
headerImage: true
tag:
- Site web
category: blog
author: amaldona
description: Et il est sous licence libre
---

<p style="text-align: center;">
  <code style="color:#666;background:transparent;font-size:large">
    · hello world ·
  </code>
</p>


Après des heures et des heures de développement, le nouveau site des élu·e·s est enfin prêt à accueillir du contenu 🚀

Il est un peu vide pour l'instant, mais vous pourrez bientôt retrouver ici toute sorte d'informations utiles pour vos études à l'UTC, tout comme des posts de vos élu·e·s pour vous tenir au courant de ce qui se fait dans les différentes instances.

Au cours des prochaines semaines, nous espérons pouvoir reprendre du contenu des sites de nos deux listes ([UTransFORMATION](https://utransformation.fr/) et [UTCo](https://www.utco.fr)) pour le remettre ici. Par exemple, nous comptons reprendre [cette série d'articles sur le fonctionnement de l'UTC](https://utransformation.fr/comprendre-lutc/).

## Ce site est le votre !

Toute contribution est la bienvenue. J'ai préparé un [tutoriel en 3 simples étapes](https://gitlab.utc.fr/elus_etu/website/-/blob/master/doc/tutoriel.md) que vous pouvez suivre si vous avez un compte UTC (votre compte CAS). Sinon, c'est un peu moins simple, mais vous pouvez toujours [m'envoyer vos contributions par mail](mailto:amaldona@etu.utc.fr?subject=[Site des élu·e·s] [Contribution]) !

Afin de permettre un partage dans les meilleures conditions, le contenu de ce site est sous licence [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr) (sauf mention contraire).

[![Creative Commons Attribution-ShareAlike 4.0 International Image]({{site.url}}/assets/images/doc/CC_BY-SA_4.0.png)](http://creativecommons.org/licenses/by-sa/4.0/)

Le code qui permet de générer ce site est sous licence [GNU AGPLv3](http://www.gnu.org/licenses/agpl-3.0.fr.html) et repose sur de nombreux autres projets libres: [Jekyll](https://jekyllrb.com/), [Indigo](https://github.com/sergiokopplin/indigo) et [GitLab](https://about.gitlab.com) pour n'en citer que 3. Il est disponible ici: <https://gitlab.utc.fr/elus_etu/website/>.

[![GNU AGPLv3 Image]({{site.url}}/assets/images/doc/AGPLv3.png)](http://www.gnu.org/licenses/agpl-3.0.fr.html)

[Plus d'infos sur le choix des licences](https://gitlab.utc.fr/elus_etu/website/-/blob/master/doc/licences.md)

## Et mes données ?

Ce site fait son mieux pour respecter votre vie privée. Les outils d'_analytics_ sont désactivés sur ce site et aucun contenu externe n'est chargé. (C'est pour ça d'ailleurs qu'on a pas besoin de mettre un de ces pop-ups embêtants que vous retrouvez sur plein de sites web).

Ce site est un site statique hébergé sur l'[instance GitLab de l'UTC](https://gitlab.utc.fr/) avec GitLab Pages. Nous avons donc accès à strictement aucune information sur nos visiteurs.
